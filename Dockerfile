# Statusbar
#
# VERSION    1.0.0

FROM daocloud.io/node:5
MAINTAINER me@yanbingbing.com

ENV HTTP_PORT 2019

COPY . /app
WORKDIR /app

RUN npm install --registry=https://registry.npm.taobao.org

EXPOSE 2019

CMD ["npm", "start"]
