const ora = require('ora');

let dbName = 'sanweishuwu';
let username = 'root';
let password = 'root123';
let dbHost = `mongodb://${username}:${password}@localhost:27017/${dbName}`;
let mongoose = require('mongoose');
exports.connect = function (request, response) {
  const spinner = ora('正在连接数据库中').start();
  mongoose.connect(dbHost,{ useNewUrlParser: true ,authSource:'admin'});
  let db = mongoose.connection;
  db.on('error', console.error.bind(console, spinner.fail('连接数据库失败')));
  db.once('open', function (callback) {
    spinner.succeed('连接数据库成功')
  });
};
